<?php

namespace YiiWidgetManager\Widget;

class Out extends \CWidget
{

	public $content = null;

	public function run()
	{
		if (null !== $this->content) {
			$this->beginWidget('\YiiWidgetManager\Widget\Templater');
			echo $this->content;
			$this->endWidget();
		}
	}

}
