<?php

namespace YiiWidgetManager\Widget;

class Templater extends \COutputProcessor {

	public $leftDelimiter = '{{';
	public $rightDelimiter = '}}';
	//
	public $widgets = null;
	protected $_defaultWidgets = [
		'loadWidget' => [
			'requiredParams' => ['id'],
			'class' => '\YiiWidgetManager\Widget\WidgetLoader',
		],
	];
	//
	protected $_funcRegexp = '[a-zA-Z_]\w*';
	//
	protected $_num_const_regexp = '(?:\-?\d+(?:\.\d+)?)';
	//
	protected $_db_qstr_regexp = '"[^"\\\\]*(?:\\\\.[^"\\\\]*)*"';
	protected $_si_qstr_regexp = '\'[^\'\\\\]*(?:\\\\.[^\'\\\\]*)*\'';
	protected $_qstrRegexp;
	//
	protected $_var_regexp;
	//
	protected $_output = null;

	public function init() {
		parent::init();

		// matches single or double quoted strings
		$this->_qstrRegexp = '(?:' . $this->_db_qstr_regexp . '|' . $this->_si_qstr_regexp . ')';

		$this->_var_regexp = '(?:' . $this->_qstrRegexp . ')';
	}

	public function processOutput($output) {

		parent::processOutput($this->processTemplate($output));
	}

	public function processTemplate($content) {

		if (null !== $this->widgets)
			$this->_defaultWidgets = array_merge($this->_defaultWidgets, $this->widgets);

		if (sizeof($this->_defaultWidgets) > 0) {

			$ldq = preg_quote($this->leftDelimiter, '~');
			$rdq = preg_quote($this->rightDelimiter, '~');

			preg_match_all("~{$ldq}\s*(.*?)\s*{$rdq}~s", $content, $_match);
			$template_tags = $_match[1];

			$compiledTags = [];
			foreach ($template_tags as $template_tag) {

				$_output = '';

				try {

					if (!preg_match('~^(?:(' . $this->_funcRegexp . '))(?:\s+(.*))?$~xs', $template_tag, $match)) {
						$this->_syntaxError("Неопознаный тэг: '{$template_tag}'");
					}

					$tag_command = $match[1];
					$tag_args = isset($match[2]) ? $match[2] : null;

					if (!isset($this->_defaultWidgets[$tag_command])) {
						$this->_syntaxError("Неопознаная команда: '{$tag_command}'");
					}

					$aParams = $this->_parse_attrs($tag_args);
					if (isset($this->_defaultWidgets[$tag_command]['defaultParams']))
						$aParams = array_merge($this->_defaultWidgets[$tag_command]['defaultParams'], $aParams);

					if (isset($this->_defaultWidgets[$tag_command]['requiredParams'])) {
						foreach ($this->_defaultWidgets[$tag_command]['requiredParams'] as $_p) {
							if (!isset($aParams[$_p]))
								$this->_syntaxError("Отсутствует обязательный параметр: '{$_p}' в тэге '{$tag_command}'");
						}
					}

					$_output .= $this->widget($this->_defaultWidgets[$tag_command]['class'], $aParams, true);
				} catch (\CException $exc) {
					$_output = "<p class=\"ntempaler-syntax-error\">{$exc->getMessage()}</p>";
				}


				$compiledTags[] = $_output;
			}

			array_walk($_match[0], create_function('&$str', '$str=\'~\'.$str.\'~\';'));
			$content = preg_replace($_match[0], $compiledTags, $content);
		}

		return $content;
	}

	protected function _parse_attrs($tag_args) {

		/* Tokenize tag attributes. */
		preg_match_all('~(?:' . $this->_qstrRegexp . ' | (?>[^"\'=\s]+)
                         )+ |
                         [=]
                        ~x', $tag_args, $match);
		$tokens = $match[0];

		$attrs = [];
		/* Parse state:
		  0 - expecting attribute name
		  1 - expecting '='
		  2 - expecting attribute value (not '=') */
		$state = 0;

		foreach ($tokens as $token) {
			switch ($state) {
				case 0:
					/* If the token is a valid identifier, we set attribute name
					  and go to state 1. */
					if (preg_match('~^\w+$~', $token)) {
						$attr_name = $token;
						$state = 1;
					} else
						$this->_syntaxError("Некорректное имя атрибута: '{$token}'");
					break;

				case 1:
					/* If the token is '=', then we go to state 2. */
					if ($token == '=') {
						$state = 2;
					} else
						$this->_syntaxError("Ожидается '=' после имени атрибута: '{$last_token}'");
					break;

				case 2:
					/* If token is not '=', we set the attribute value and go to
					  state 0. */
					if ($token != '=') {
						/* We booleanize the token if it's a non-quoted possible
						  boolean value. */
						if (preg_match('~^(on|yes|true)$~', $token)) {
							$token = true;
						} else if (preg_match('~^(off|no|false)$~', $token)) {
							$token = false;
						} else if ($token == 'null') {
							$token = null;
						}
						/* else if (preg_match('~^' . $this->_num_const_regexp . '|0[xX][0-9a-fA-F]+$~', $token)) {

						  } else if (!preg_match('~^' . $this->_var_regexp . '$~', $token)) {

						  } */

						$attrs[$attr_name] = trim($token, '"\'');
						$state = 0;
					} else
						$this->_syntaxError("'=' не может быть значением атрибута");
					break;
			}
			$last_token = $token;
		}

		if ($state != 0) {
			if ($state == 1) {
				$this->_syntaxError("Ожидается '=' после имени атрибута: '{$last_token}'");
			} else {
				$this->_syntaxError("Отсутствует значение атрибута");
			}
		}

		return $attrs;
	}

	protected function _syntaxError($message, $code = 0) {

		throw new \CException('Синтаксическая ошибка: ' . $message, $code);
	}

}
