<?php

namespace YiiWidgetManager\Widget;

class Navigation extends \CWidget
{

	public $navigation_id = null;
	public $depth = 0;
	public $cssClass = null;

	public $id = null;

	public function init()
	{
		parent::init();

		if ((int)$this->navigation_id == 0)
			throw new \CException('Widget must have navigation_id.');

		$this->depth = (int)$this->depth;
	}

	public function run()
	{

		$criteria = new \CDbCriteria;

		if ($this->depth > 0) {
			$criteria->addCondition('`level` <= :level');
			$criteria->params[':level'] = $this->depth;
		}

		$aItems = \NavigationItem::model()->prepareHierarchy(\NavigationItem::model()->published()->defaultOrder()->navigationScope($this->navigation_id)->findAll($criteria));

		if ([] !== $aItems) {

			$aParams = ['items' => $aItems,];

			if (!empty($this->id))
				$aParams['id'] = $this->id;

			if (!empty($this->cssClass))
				$aParams['htmlOptions'] = ['class' => $this->cssClass];

			$this->widget('\YiiNavigationComponent\Widget\Menu', $aParams);
		}
	}

}
