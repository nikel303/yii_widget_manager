<?php

namespace YiiWidgetManager\Widget;

class WidgetContainer extends \CWidget {

    public $widgets = [];
    public $widgetDecorator = null;
    public $containerDecorator = null;

    public function init() {
        parent::init();

        if (!$this->getId(false))
            throw new \CException('Widget container must have an id.');

        foreach (\Yii::app()->widgetManager->loadContainerWidgets($this->getId(false)) as $itm)
            $this->widgets[] = $itm;
    }

    public function run() {
        parent::run();

        if (sizeof($this->widgets) > 0) {

            if (null !== $this->containerDecorator)
                $this->beginContent($this->containerDecorator, ['containerId' => $this->getId(false)]);

            $useDecorator = null !== $this->widgetDecorator;

            foreach ($this->widgets as $itm) {

                $out = trim($this->widget($itm['widget'], isset($itm['widgetParams']) && is_array($itm['widgetParams']) ? $itm['widgetParams'] : [], true));

                if (!empty($out)) {
                    if ($useDecorator)
                        $this->beginContent($this->widgetDecorator, [
                            'containerId' => $this->getId(false),
                            'id' => isset($itm['id']) ? $itm['id'] : null,
                            'title' => isset($itm['title']) ? $itm['title'] : null,
                            'class' => isset($itm['class']) ? $itm['class'] : null,
                        ]);

                    echo $out;

                    if ($useDecorator)
                        $this->endWidget();
                }
            }

            if (null !== $this->containerDecorator)
                $this->endWidget();
        }
    }

}
