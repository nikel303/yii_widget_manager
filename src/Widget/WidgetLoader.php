<?php

namespace YiiWidgetManager\Widget;

class WidgetLoader extends \CWidget {

	public $decorator = null;

	public function init() {
		parent::init();

		if (!$this->getId(false))
			throw new \CException('Widget container must have an id.');
	}

	public function run() {
		parent::run();

		if (null !== ($widget = \CActiveRecord::model(\Yii::app()->widgetManager->widgetModel)->published()->findByAttributes(['alias' => $this->getId(false)]))) {

			$typeModel = $widget->getTypeModel();

			$cssClass = !empty($widget->cssClass) ? $widget->cssClass : 'widgetType-' . mb_strtolower(get_class($widget->arType)) . ' widgetAlias-' . preg_replace('/[^a-z0-9_]/ui', '-', $widget->alias);
			$params = !empty($widget->params) && false !== ($data = unserialize($widget->params)) && is_array($data) ? $data : [];

			$useDecorator = (null !== $this->decorator);

			if ($useDecorator)
				$this->beginContent($this->decorator, [
					'title' => !empty($widget->title) ? $widget->title : null,
					'class' => !empty($cssClass) ? $cssClass : null,
				]);

			$this->widget($typeModel->getWidgetClass(), $params);

			if ($useDecorator)
				$this->endWidget();
		}
	}

}
