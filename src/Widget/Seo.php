<?php

namespace YiiWidgetManager\Widget;

class Seo extends \CWidget
{

    public $h1;
    public $title;
    public $description;
    public $keywords;
    public $canonical;
    //
    public $og_site_name;
    public $og_title;
    public $og_description;

    public function init()
    {

    }

    public function run()
    {
        
        $controller = $this->getController();

        if (!empty($this->title))
            $controller->pageTitle = $this->title;

        if (!empty($this->h1))
            $controller->pageH1 = $this->h1;

        if (!empty($this->description))
            $controller->pageDescription = $this->description;

        if (!empty($this->keywords))
            $controller->pageKeywords = $this->keywords;

        if (!empty($this->canonical))
            $controller->pageCanonical = $this->canonical;

        if (!empty($this->og_site_name))
            $controller->pageOGSiteName = $this->og_site_name;

        if (!empty($this->og_title))
            $controller->pageOGTitle = $this->og_title;
        elseif (!empty($this->title))
            $controller->pageOGTitle = $this->title;

        if (!empty($this->og_description))
            $controller->pageOGDescription = $this->og_description;
        elseif (!empty($this->description))
            $controller->pageOGDescription = $this->description;
    }

}