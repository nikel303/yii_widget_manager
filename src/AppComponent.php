<?php

namespace YiiWidgetManager;

class AppComponent extends \CApplicationComponent
{

    public $widgetModel = 'Widget';
    public $widgetTypeModel = 'WidgetType';
    public $widgetContainerModel = 'WidgetContainer';
    public $widgetContainerRelModel = 'WidgetContainerRel';

    protected $_containers;
    protected $_types;
    //
    protected static $_widgetsCache = null;

    static public function getCurrentUrl($refresh = false)
    {

        static $_url = null;

        if (null === $_url || $refresh) {
            $_url = \Yii::app()->request->getUrl();
            $_url = preg_replace("/\/+/", '/', $_url);
            $_url = preg_replace("/^\/(.*)\/?$/U", '\\1', $_url);
        }

        return $_url;
    }

    protected function _fetchTypes()
    {

        $out = [];
        foreach (\CActiveRecord::model($this->widgetTypeModel)->published()->defaultOrder()->findAll() as $itm)
            $out[(int)$itm->id] = $itm;

        return $out;
    }

    public function getTypes($refresh = false)
    {

        if (null === $this->_types || $refresh)
            $this->_types = $this->_fetchTypes();

        return $this->_types;
    }

    public function getTypeModel($config, $params = [])
    {

        if (is_string($config))
            $config = ['class' => $config];

        try {

            $component = \Yii::createComponent(\CMap::mergeArray($config, $params));
        } catch (\CException $e) {

            return false;
        }

        return $component;
    }

    public function getTypeModelById($id, $params = [])
    {

        $types = $this->getTypes();
        if (!isset($types[$id]))
            throw new \CException(__CLASS__ . " type \"{$id}\" is undefined.");

        return $types[$id]->getTypeModel($params);
    }

    public function getTypesOptions()
    {

        static $options = null;

        if (null === $options) {

            $types = $this->getTypes();
            $options = [];

            foreach ($types as $id => $widgetModel) {

                $model = $widgetModel->getTypeModel();
                if (false === $model)
                    continue;

                $options[$id] = $model->getTypeTitle();
            }
        }

        return $options;
    }

    protected function _fetchContainers()
    {

        $out = [];
        foreach (\CActiveRecord::model($this->widgetContainerModel)->published()->defaultOrder()->findAll() as $itm)
            $out[(int)$itm->id] = $itm;

        return $out;
    }

    public function getContainers($refresh = false)
    {

        if (null === $this->_containers || $refresh)
            $this->_containers = $this->_fetchContainers();

        return $this->_containers;
    }

    public function getContainersOptions()
    {

        static $options = null;

        if (null === $options)
            $options = array_map(function ($v) {
                return $v->title;
            }, $this->getContainers());

        return $options;
    }

    public function loadContainerWidgets($containerAlias)
    {

        if (!isset(self::$_widgetsCache[$containerAlias])) {

            $_url = self::getCurrentUrl();
            $_cacheId = $_url . ' ' . $containerAlias;

            if (false === (self::$_widgetsCache[$containerAlias] = \Yii::app()->cache[$_cacheId])) {

                self::$_widgetsCache[$containerAlias] = [];

                $criteria = new \CDbCriteria;

                $criteria->with['arContainer'] = [
                    'select' => false,
                    'joinType' => 'INNER JOIN',
                    'together' => true,
                    //
                    'on' => 'alias=:alias',
                    'params' => [
                        ':alias' => $containerAlias,
                    ],
                    'scopes' => [
                        'published',
                    ],
                ];

                $criteria->with['arWidget'] = [
                    'scopes' => 'published',
                ];

                $criteria->scopes = [
                    'published',
                    'defaultOrder',
                ];

                foreach (\CActiveRecord::model($this->widgetContainerRelModel)->findAll($criteria) as $itm) {

                    if (!$itm->getIsVisible($_url) || false === ($widgetModel = $itm->arWidget->getTypeModel()))
                        continue;

                    self::$_widgetsCache[$containerAlias][] = [
                        'id' => $itm->arWidget->id,
                        'title' => $itm->arWidget->title,
                        'class' => !empty($itm->arWidget->css_class) ? $itm->arWidget->css_class : 'widgetType-' . $itm->arWidget->type_id . ' widgetAlias-' . preg_replace('/[^a-z0-9_]/ui', '-', $itm->arWidget->alias),
                        'widget' => $widgetModel->getWidgetClass(),
                        'widgetParams' => !empty($itm->arWidget->params) && false !== ($data = unserialize($itm->arWidget->params)) && is_array($data) ? $data : [],
                    ];
                }

                \Yii::app()->cache[$_cacheId] = self::$_widgetsCache[$containerAlias];
            }
        }

        return self::$_widgetsCache[$containerAlias];
    }

    public static function matchUrlRule($sUrlRule, $sUrl)
    {

        if (is_array($sUrlRule)) {

            foreach ($sUrlRule as $key => $pattern) {

                $value = '';
                switch ($key) {

                    case '_c':

                        $value = \Yii::app()->getController()->getId();
                        break;

                    case '_a':

                        $value = \Yii::app()->getController()->getAction()->getId();
                        break;

                    default:

                        if (isset($_GET[$key]))
                            $value = $_GET[$key];
                }

                if (!(bool)preg_match('/^' . $pattern . '$/', $value))
                    return false;
            }

            return true;
        }

        if ($sUrlRule == '<front>') {

            $controller = \Yii::app()->getController();
            $defaultControllerId = \Yii::app()->defaultController;

            return $controller->id == $defaultControllerId && $controller->action->id === $controller->defaultAction;
        }

        if (!preg_match('/^' . $sUrlRule . '$/', $sUrl, $r)) {
            return false;
        }

        return $r;
    }

}
