<?php

namespace YiiWidgetManager\Type;

class Text extends Type {

    public $content;

    public function getWidgetClass() {
        return '\YiiWidgetManager\Widget\Out';
    }

    public function getTypeTitle() {
        return 'Текcтовой блок';
    }

    public function rules() {
        return [
            ['content', 'safe'],
        ];
    }

    public function attributeLabels() {
        return [
            'content' => 'Содержание',
        ];
    }

    public function getData() {
        return [
            'params' => serialize($this->attributes),
        ];
    }

    public function attributeWidgets() {
        return [
            'content' => [
                'type' => 'textarea',
                'htmlOptions' => [
                    'rows' => 6,
                    'cols' => 50,
                ],
            ],
        ];
    }

}
