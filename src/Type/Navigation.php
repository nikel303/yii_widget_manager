<?php

namespace YiiWidgetManager\Type;

class Navigation extends Type {

    public $navigation_id;
    public $depth = 0;
    public $cssClass;
    public $id;

    public function getWidgetClass() {
	    return '\YiiWidgetManager\Widget\Navigation';
    }

    public function getTypeTitle() {
        return 'Меню навигации';
    }

    public function rules() {
        return [
            ['navigation_id, depth', 'required'],
            ['navigation_id, depth', 'numerical', 'integerOnly' => true],
            ['navigation_id', 'exist', 'attributeName' => 'id', 'className' => 'Navigation'],
            ['cssClass, id', 'length', 'max' => 255],
        ];
    }

    public function attributeLabels() {
        return [
            'navigation_id' => 'Меню навигации',
            'depth' => 'Кол-во уровней',
            'cssClass' => 'Css класс списка меню',
            'id' => 'ID элемента списка меню',
        ];
    }

    public function attributeWidgets() {
        return [
            'navigation_id' => [
                'type' => 'dropdownlist',
                'data' => \CHtml::listData(\Navigation::model()->defaultOrder()->findAll(), 'id', 'admin_title'),
                'htmlOptions' => [
                    'empty' => '~',
                ],
            ],
            'depth' => [
                'type' => 'text',
                'htmlOptions' => [
                    'size' => 20,
                    'maxlength' => 10,
                ],
            ],
            'cssClass' => [
                'type' => 'text',
                'htmlOptions' => [
                    'size' => 60,
                    'maxlength' => 255,
                ],
            ],
            'id' => [
                'type' => 'text',
                'htmlOptions' => [
                    'size' => 60,
                    'maxlength' => 255,
                ],
            ],
        ];
    }

    public function getData() {

        $aData = [];

        if (!empty($this->navigation_id) && null !== ($model = \Navigation::model()->findByPk($this->navigation_id))) {
            $aData = [
                'params' => serialize([
                    'navigation_id' => $model->id,
                    'depth' => (int)$this->depth,
                    'cssClass' => $this->cssClass,
                    'id' => $this->id,
                ]),
            ];
        }

        return $aData;
    }

}
