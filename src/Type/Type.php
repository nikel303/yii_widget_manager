<?php

namespace YiiWidgetManager\Type;

/**
 * Description of NWidgetManagerWidgetType
 */
abstract class Type extends \CFormModel {

	abstract public function getWidgetClass();

	abstract public function getTypeTitle();

	abstract public function getData();

	abstract public function attributeWidgets();
}
