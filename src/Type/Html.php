<?php

namespace YiiWidgetManager\Type;

class Html extends Type {

	public $content;

	public function getWidgetClass() {
		return '\YiiWidgetManager\Widget\Out';
	}

	public function rules() {
		return [
			['content', 'safe'],
		];
	}

	public function attributeLabels() {
		return [
			'content' => 'Содержание',
		];
	}

	public function getData() {
		return [
			'params' => serialize($this->attributes),
		];
	}

	public function getTypeTitle() {
		return 'Текcтовой блок (html)';
	}

	public function attributeWidgets() {
		return [
			'content' => [
				'type' => 'ckeditor',
				'htmlOptions' => [],
			],
		];
	}

}
