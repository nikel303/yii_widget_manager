<?php

namespace YiiWidgetManager\Type;

class Seo extends Type
{

    public $h1;
    public $title;
    public $description;
    public $keywords;
    public $canonical;
    //
    public $og_site_name;
    public $og_title;
    public $og_description;

    public function getTypeTitle()
    {
        return 'Настройка SEO тэгов';
    }

    public function getWidgetClass()
    {
        return '\YiiWidgetManager\Widget\Seo';
    }

    public function rules()
    {
        return [
            ['h1, title, description, keywords, canonical, og_site_name, og_title, og_description', 'filter', 'filter' => 'trim'],
            ['h1, title, description, keywords, canonical', 'length', 'max' => 500],
            ['og_site_name, og_title', 'length', 'max' => 255],
            ['og_description', 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'h1' => 'Заголовок страницы [h1]',
            'title' => 'Заголовок страницы [title]',
            'description' => 'Описание страницы',
            'keywords' => 'Ключевые слова',
            'canonical' => 'Канонический URL',
            //
            'og_site_name' => 'OG::Название сайта',
            'og_title' => 'OG::Заголовок',
            'og_description' => 'OG::Описание',
        ];
    }

    public function attributeWidgets()
    {
        return [
            'h1' => [
                'type' => 'text',
                'htmlOptions' => [
                    'size' => 50,
                    'maxlength' => 500,
                ],
            ],
            'title' => [
                'type' => 'text',
                'htmlOptions' => [
                    'size' => 50,
                    'maxlength' => 500,
                ],
            ],
            'description' => [
                'type' => 'textarea',
                'htmlOptions' => [
                    'size' => 50,
                    'maxlength' => 500,
                ],
            ],
            'keywords' => [
                'type' => 'textarea',
                'htmlOptions' => [
                    'size' => 50,
                    'maxlength' => 500,
                ],
            ],
            'canonical' => [
                'type' => 'text',
                'htmlOptions' => [
                    'size' => 50,
                    'maxlength' => 500,
                ],
            ],
            'og_site_name' => [
                'type' => 'text',
                'htmlOptions' => [
                    'size' => 50,
                    'maxlength' => 255,
                ],
            ],
            'og_title' => [
                'type' => 'text',
                'htmlOptions' => [
                    'size' => 50,
                    'maxlength' => 255,
                ],
            ],
            'og_description' => [
                'type' => 'textarea',
                'htmlOptions' => [
                    'size' => 50,
                ],
            ],
        ];
    }

    public function getData()
    {

        $aData = [
            'params' => serialize($this->attributes),
        ];

        return $aData;
    }

}
